<?php

namespace Imsdata;

class SSEdPortal extends Curl 
{

    const URL_LOGIN_FORM = "https://ims.ssd.sk/login/";
    const URL_LOGIN_POST = "https://ims.ssd.sk/api/account/login";
    const URL_CHART_DATA = "https://ims.ssd.sk/api/consumption-production/profile-data/chart-data";
    const URL_POINT_OF_DELIVERIES_LIST = "https://ims.ssd.sk/api/consumption-production/profile-data/get-points-of-delivery";
    const DATETIME_FORMAT = "Y-m-d\TH:i:s\Z";

    public function __construct($proxy=null)
    {
        parent::__construct();
        if($proxy) $this->proxy = $proxy;
    }

    public function login(string $login, string $pass)
    {
        return $this->post(
                    self::URL_LOGIN_POST,
                    json_encode([ "username" => $login, "password" => $pass ])
               );
    }

    public function getPointsOfDelivery()
    {
        $data = $this->get(self::URL_POINT_OF_DELIVERIES_LIST);
        return json_decode($data);
    }

    public function getTimeData(\DateTime $dateFrom, \DateTime $dateTo, object $pointOfDelivery)
    {
        $response = $this->post(
                        self::URL_CHART_DATA, 
                        json_encode([ "pointOfDeliveryId" => $pointOfDelivery->value, "pointOfDeliveryText" => $pointOfDelivery->text, "validFromDate" => $dateFrom->format(self::DATETIME_FORMAT), "validToDate" => $dateTo->format(self::DATETIME_FORMAT) ])
                    );

        if($response) {
            return json_decode($response);
        }
        else {
            throw \Exception ("SSEdPortal getTimeData(): no data found");
        }

        return null;
    }

}
