<?php
require "../src/Curl.php";
require "../src/SSEdPortal.php";


$sp = new Imsdata\SSEdPortal();

// your login details from invoice/contract with energy provider (your energy unit have to be physically located in middle Slovakia region)
$sp->login("login", "password");

// get data for last 24 hours
$dtNow = new \DateTime();
$data = $sp->getProfileData((clone $dtNow)->modify("-1 day"), $dtNow);

// jsut dump it to see if it works
var_dump($data);
?>