# imsdata
EN:
PHP class(es) for scrapping energetics data from inteligent meters (IMS) of Slovak energy distribution providers via IMS portals.

SK:
PHP triedy pre získanie energetických dát z inteligentných meračov (IMS) od slovenských distribútorov energií cez IMS portály.


# Použitie / usage:

```
use Imsdata;
```

```
$ims = new SSEdPortal();
$ims->login("login", "password");

$dtFrom = new \DateTime();
$dtFrom->modify("-3 days");
$dtTo = new \DateTime();

$imsdata = $ims->getProfileData($dtFrom, $dtTo);
```

Uvedený príklad získa dáta za predchádzajúce 3 dni. Dáta z IMS merača sa do portálu spracujú raz denne. Údaje z dnešného dňa sú dostupné až po polnoci.


# Podporované:

## 1. Stredoslovenská distribučná a.s. (SSD): portál IMS
[https://ims.ssd.sk]

### Návod na vytvorenie používateľského konta na portáli ims.sse-d.sk:
1. Vyplňte registračný formulár na stránke:
[https://ims.ssd.sk/public/new-registration]
2. Používateľské (prihlasovacie) meno musí byť unikátne (vyberáte si ho podľa seba).
3. EIC kód získate z faktúry vášho príslušného koncového predajcu el. energie. Je to 16 miestny kód zložený z písmen a čísiel.
4. Číslo elektromeru nájdete buď na samotnom merači alebo na doklade o výmene merača distribučnou spoločnosťou. Obsahuje minimálne 8 znakov. Ak ich je menej, treba ho doplniť úvodnými nulami (napr: 00193847).
5. Po úspešnej registrácii netreba nič viac overovať. Uvedené prihlasovacie údaje treba vyplniť do volania metódy SSEdPortal->login(prihlasovacie_meno, heslo).



*Classy na získanie dát od ďalších distribučných spoločností (ZSE, VSE) snáď pribudnú v budúcnosti. V prípade záujmu ma prosím kontaktujte a skúsime nájsť spôsob, ako to sprevádzkovať.*